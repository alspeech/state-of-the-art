## this Repository contains a brief summary of some  SOTA papers and tools which are useful for automated speech translation such as :


1. [Multilingual End-to-End Speech Translation](https://arxiv.org/abs/1910.00254)
2. [Listen and Translate: A Proof of Concept for End-to-End Speech-to-Text Translation](https://hal.archives-ouvertes.fr/hal-01408086/document)
3. [MuST-C: a Multilingual Speech Translation Corpus](https://www.aclweb.org/anthology/N19-1202/)
4. [FairSeq](https://arxiv.org/abs/1904.01038)
5. [attention is all you need](https://arxiv.org/abs/1706.03762)
6. [BERT](https://arxiv.org/pdf/1810.04805.pdf)